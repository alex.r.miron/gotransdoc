package service

import (
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"io/fs"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"os/exec"
	"path"
	"path/filepath"
	"strings"
	"time"
)

type ServerConfig struct {
	SSLCert    string
	SSLKey     string
	ServerHost string
	ServerPort string
	ServerPath string
}

var Settings = ServerConfig{}

type FileCommand struct {
	Command string
	Args    []string
	Stdout string
}

func parseManifest(path string) ([]FileCommand, error) {
	var manifest []FileCommand
	data, err := ioutil.ReadFile(path)
	if err != nil {
		return nil, err
	}
	json.Unmarshal(data, &manifest)
	return manifest, nil

}

type AppHandler func(http.ResponseWriter, *http.Request) error

var startTime time.Time

func init() {
	startTime = time.Now().Local()
}

type AuthToken struct {
	Expires string
}

func Authorizer(fn AppHandler) AppHandler {
	return func(w http.ResponseWriter, r *http.Request) error {
		if !HaveAuthorizationInfo() {
			return fn(w, r)
		}
		auth := r.Header.Get("Authorization")
		if len(auth) == 0 {
			return errors.New("No Authorization header.")
		}
		verified, err := DecodeVerify(auth)
		if err != nil || len(verified) == 0 {
			return errors.New("Not Authorized.")
		}
		var token AuthToken
		err = json.Unmarshal(verified, &token)
		if err != nil || len(verified) == 0 {
			return errors.New("Bad token format.")
		}

		if token.Expires != "0000-00-00" {

			expires, err := time.Parse("2006-01-02", token.Expires)
			if err != nil {
				return errors.New(fmt.Sprintf("Bad time format: %s", token.Expires))

			}
			delta := expires.Sub(time.Now())
			if delta < 0 {
				return errors.New(fmt.Sprintf("Token expired at %s.", token.Expires))
			}
		}

		return fn(w, r)
	}
}

func (fn AppHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	if err := fn(w, r); err != nil {
		log.Printf("Error: %s", err)
		http.Error(w, err.Error(), 500)
	}
}

func SaveInput(name string, fh io.Reader) (string, string, error) {

	tempd, err := ioutil.TempDir("", "")
	if err != nil {
		return "", "", err
	}

	fullFname := path.Join(tempd, name)

	log.Printf("Saving %s", fullFname)
	outf, err := os.Create(fullFname)
	if err != nil {
		return tempd, "", err
	}
	defer outf.Close()
	_, err = outf.ReadFrom(fh)

	return tempd, name, err

}

func postFile(w http.ResponseWriter, r *http.Request) (string, string, error) {
	r.ParseMultipartForm(500000)
	if r.MultipartForm == nil {
		return "", "", errors.New("You should POST a multipart form")
	}
	headers, ok := r.MultipartForm.File["zipped_source"]
	if !ok || len(headers) != 1 {
		return "", "", errors.New("There should be a file upload for key zipped_source")
	}

	fh, err := headers[0].Open()
	if err != nil {
		return "", "", err
	}
	defer fh.Close()

	return SaveInput(headers[0].Filename, fh)
}

func GetManifest(dir string, fname string) ([]FileCommand, error) {
	var manifestData []FileCommand

	cmd := exec.Command("unzip", fname)
	cmd.Dir = dir
	err := cmd.Run()

	if err != nil {
		return manifestData, err
	}
	_ = os.Remove(path.Join(dir, fname))

	filepath.WalkDir(dir, func(path string, d fs.DirEntry, err error) error {
		if d.Name() == "command_manifest.json" {
			data, err := parseManifest(path)
			if err == nil {
				manifestData = data
			}
			return fs.SkipDir
		}
		return nil
	})
	return manifestData, nil
}

func Cleanup(dir string) {
	if len(dir) < 4 {
		return
	}
	if strings.Contains(dir, "..") {
		return
	}
	log.Printf("Removing %s", dir)
	err := os.RemoveAll(dir)
	if err != nil {
		log.Printf("Error %s", err)
	}
}

func PackOutput(dir string) (io.ReadCloser, func(), error) {
	cmd := exec.Command("zip", "-r", "-", ".")
	cmd.Dir = dir
	stdout, _ := cmd.StdoutPipe()
	err := cmd.Start()
	if err != nil {
		return stdout, nil, err
	}
	return stdout, func() { cmd.Wait() }, nil
}

func RunCmd(dir string, m *FileCommand, commandMap map[string][]string) error {
	var cwd string
	filepath.WalkDir(dir, func(path string, d fs.DirEntry, err error) error {
		for _, arg := range m.Args {
			if d.Name() == arg {
				cwd, _ = filepath.Split(path)
				return fs.SkipDir
			}
		}
		return nil
	})
	if len(cwd) == 0 {
		return errors.New("no such file ")
	}

	command, ok := commandMap[m.Command]
	if !ok {
		return errors.New(fmt.Sprintf("command not allowed: %s", m.Command))
	}
	log.Printf("Executing %s", append(command, m.Args...))
	cmd := exec.Command(command[0], append(command[1:], m.Args...)...)
	cmd.Dir = cwd
	if len(m.Stdout)>0 {
		log.Printf("Stdout: %s", m.Stdout)
		outHandle,err := os.Create(filepath.Join(cwd,m.Stdout))
		if err == nil {
			defer outHandle.Close()
			cmd.Stdout = outHandle
		}else {
			log.Printf("Error: %s", err)
		}
	}
	err := cmd.Run()
	return err
}

func serveOutput(w http.ResponseWriter, fh io.ReadCloser, fname string) error {
	w.Header().Set("Content-Disposition", fmt.Sprintf("attachment; filename=%s", fname))
	w.Header().Set("Content-Type", "application/zip")
	_, err := io.Copy(w, fh)
	return err

}

func HandlePOST(w http.ResponseWriter, r *http.Request, worker func(string, string) (io.ReadCloser, func(), string, error)) error {

	dir, fname, err := postFile(w, r)
	defer Cleanup(dir)

	if err != nil {
		return err
	}

	fh, cb, outFname, err := worker(dir, fname)
	if err != nil {
		return err
	}
	err = serveOutput(w, fh, outFname)
	if cb != nil {
		cb()
	}
	return err

}

type RouteInfo struct {
	Path    string
	Handler func(http.ResponseWriter, *http.Request) error
	Secure  bool
}

func HandleHealth(w http.ResponseWriter, r *http.Request) error {
	fmt.Fprintf(w, "ok. running since %s\n", startTime)
	return nil
}

func Serve(settings *ServerConfig, routes []RouteInfo) {
	for _, route := range routes {
		if route.Secure {
			http.Handle(settings.ServerPath+route.Path, Authorizer(AppHandler(route.Handler)))
		} else {
			http.Handle(settings.ServerPath+route.Path, AppHandler(route.Handler))
		}
	}
	log.Printf("Starting web service on %s:%s", settings.ServerHost, settings.ServerPort)
	if len(settings.SSLKey) > 0 && len(settings.SSLCert) > 0 {
		log.Printf("Using ssl key:%s, cert:%s", settings.SSLKey, settings.SSLCert)
		log.Fatal(http.ListenAndServeTLS(settings.ServerHost+":"+settings.ServerPort, settings.SSLCert, settings.SSLKey, nil))
	} else {
		log.Fatal(http.ListenAndServe(settings.ServerHost+":"+settings.ServerPort, nil))
	}
}
