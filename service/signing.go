package service

import (
	"crypto"
	"crypto/rand"
	"crypto/rsa"
	"crypto/sha256"
	"crypto/x509"
	"encoding/base64"
	"encoding/pem"
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"strings"
)

var privateRSAKey *rsa.PrivateKey
var publicRSAKeys = []*rsa.PublicKey{}

func loadData(contentOrPath string) []byte {
	contentOrPath = strings.TrimSpace(contentOrPath)

	if len(contentOrPath) == 0 {
		return nil
	}

	if strings.Contains(contentOrPath, "----BEGIN") {
		return []byte(contentOrPath)
	}
	info, err := os.Stat(contentOrPath)
	if err != nil || info.IsDir() {
		return nil
	}

	data, err := ioutil.ReadFile(contentOrPath)
	if err != nil {
		return nil
	}
	log.Printf("Loaded key from %s", contentOrPath)
	return data
}

func addPrivateKeyFromPEM(pemString string) {
	data := loadData(pemString)
	if data == nil {
		return
	}
	block, err := pem.Decode(data)
	if block != nil && err != nil {
		priv, _ := x509.ParsePKCS1PrivateKey(block.Bytes)
		if priv != nil {
			privateRSAKey = priv
		}
	}
}

func addPublicKeyFromPEM(pemString string) {
	data := loadData(pemString)
	if data == nil {
		return
	}
	block, err := pem.Decode(data)
	if block != nil && err != nil {
		pub, _ := x509.ParsePKIXPublicKey(block.Bytes)
		if pub != nil {
			if pubKey, ok := pub.(*rsa.PublicKey); ok {
				publicRSAKeys = append(publicRSAKeys, pubKey)
			}
		}
	}
}

func init() {
	pubkeys := strings.Split(os.Getenv("RSA_PUBLIC_KEY"), ",")
	for _, pubkey := range pubkeys {
		addPublicKeyFromPEM(pubkey)
	}
	addPrivateKeyFromPEM(os.Getenv("RSA_PRIVATE_KEY"))
}

func Sign(message []byte) (string, error) {
	if privateRSAKey == nil {
		return "", errors.New("no private key.")
	}
	tosign := []byte(base64.StdEncoding.EncodeToString(message))

	sum := sha256.Sum256(tosign)
	signature, err := rsa.SignPKCS1v15(rand.Reader, privateRSAKey, crypto.SHA256, sum[:])
	if err != nil {
		return "", err
	}
	encoded := base64.StdEncoding.EncodeToString(signature)
	return fmt.Sprintf("%s.%s\n", tosign, encoded), nil
}

func HaveAuthorizationInfo() bool {
	return len(publicRSAKeys) > 0
}

func DecodeVerify(message string) ([]byte, error) {
	if len(publicRSAKeys) == 0 {
		return nil, errors.New("no public keys.")
	}
	components := strings.Split(strings.TrimSpace(message), ".")
	if len(components) != 2 {
		return nil, errors.New("bad string to verify.")
	}

	tosign := []byte(components[0])
	signature, err := base64.StdEncoding.DecodeString(components[1])
	if err != nil {
		return nil, errors.New("could not decode signature.")
	}

	sum := sha256.Sum256(tosign)
	has := false
	for _, pubKey := range publicRSAKeys {
		err = rsa.VerifyPKCS1v15(pubKey, crypto.SHA256, sum[:], signature)
		if err == nil {
			has = true
			break
		}

	}
	if !has {
		return nil, errors.New("bad signature.")
	}
	decoded, err := base64.StdEncoding.DecodeString(components[0])
	if err != nil {
		return nil, errors.New("could not decode message.")
	}
	return decoded, nil
}
