package ssconvert

import (
	"errors"
	"fmt"
	"gitlab.com/alex.r.miron/gotransdoc/service"
	"io"
	"io/fs"
	"net/http"
	"path/filepath"
	"strings"
)

var excelCommandMap = map[string][]string{
	"excel_to_csv": []string{"ssconvert", "-S"},
	"ssconvert":    []string{"ssconvert", "-S"},
}

func getDefaultManifestData(dir string) []service.FileCommand {
	var manifestData []service.FileCommand

	filepath.WalkDir(dir, func(path string, d fs.DirEntry, err error) error {
		name := d.Name()
		ext := strings.ToLower(filepath.Ext(name))
		if ext == ".xlsx" || ext == ".xls" {
			manifestData = append(manifestData, service.FileCommand{"excel_to_csv", []string{name, fmt.Sprintf("%s.%%n.%%s.csv", name[0:len(name)-len(ext)])},""})
		}
		return nil
	})
	return manifestData
}

func runExcelCompiler(dir string, fname string) (io.ReadCloser, func(), string, error) {

	manifestData, err := service.GetManifest(dir, fname)

	if err != nil {
		return nil, nil, fname, err
	}
	if len(manifestData) == 0 {
		manifestData = getDefaultManifestData(dir)
	}

	if len(manifestData) == 0 {
		return nil, nil, fname, errors.New("could not determine what to do.")
	}

	for _, command := range manifestData {
		err := service.RunCmd(dir, &command, excelCommandMap)
		if err != nil {
			return nil, nil, fname, err
		}
	}

	fh, fn, err := service.PackOutput(dir)
	return fh, fn, fname, err
}

func HandleConvert(w http.ResponseWriter, r *http.Request) error {
	if r.Method == "POST" {
		return service.HandlePOST(w, r, runExcelCompiler)
	}
	return nil
}
