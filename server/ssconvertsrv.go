package main

import (
	"gitlab.com/alex.r.miron/gotransdoc/ssconvert"
	"gitlab.com/alex.r.miron/gotransdoc/libreoffice"
	"gitlab.com/alex.r.miron/gotransdoc/tika"
	"gitlab.com/alex.r.miron/gotransdoc/service"
	"os"
)

func main() {

	service.Settings.SSLCert = os.Getenv("SERVER_SSL_CERT")
	service.Settings.SSLKey = os.Getenv("SERVER_SSL_KEY")
	service.Settings.ServerHost = os.Getenv("SERVER_HOST")
	service.Settings.ServerPort = os.Getenv("SERVER_PORT")
	service.Settings.ServerPath = os.Getenv("SERVER_PATH")

	service.Serve(&service.Settings, []service.RouteInfo{
		service.RouteInfo{"/api/ssconvert/", ssconvert.HandleConvert, true},
		service.RouteInfo{"/api/libreoffice/", libreoffice.HandleConvert, true},
		service.RouteInfo{"/api/tika/", tika.HandleConvert, true},
		service.RouteInfo{"/api/health/", service.HandleHealth, false},
	})
}
