package main

import (
	"os"
	"bytes"
	"gitlab.com/alex.r.miron/gotransdoc/latex"
	"gitlab.com/alex.r.miron/gotransdoc/service"
	"io"
	"io/fs"
	"mime/multipart"
	"net/http"
	"net/http/httptest"
	"path/filepath"
	"testing"
)

func TestServer(t *testing.T) {
	server := httptest.NewTLSServer(service.Authorizer(service.AppHandler(latex.HandleLatex)))
	defer server.Close()

	client := server.Client()

	var buffer bytes.Buffer
	w := multipart.NewWriter(&buffer)
	writer, err := w.CreateFormFile("zipped_source", "helloworld.zip")
	if err != nil {
		t.Errorf("%s", err)
		return
	}

	fh, post, err := service.PackOutput("../samples/helloworld")
	if err != nil {
		t.Errorf("%s", err)
		return
	}
	_, err = io.Copy(writer, fh)
	post()
	w.Close()

	request, _ := http.NewRequest("POST", server.URL, &buffer)
	request.Header.Set("Content-Type", w.FormDataContentType())
	request.Header.Set("Authorization", os.Getenv("CLIENT_SECRET"))

	res, err := client.Do(request)
	if err != nil {
		t.Errorf("%s", err)
		return
	}

	if res.StatusCode != http.StatusOK {
		t.Errorf("Bad status %d", res.StatusCode)
		return
	}

	dir, _, err := service.SaveInput("helloworld_output.zip", res.Body)
	res.Body.Close()
	defer service.Cleanup(dir)
	if err != nil {
		t.Errorf("%s", err)
		return
	}

	commands,_ := service.GetManifest(dir, "helloworld_output.zip")
	if len(commands) != 4 {
		t.Errorf("Commands don't match: %s", commands)
	}

	hasPDF := false
	hasBBL := false
	hasLOG := false
	filepath.WalkDir(dir, func(path string, d fs.DirEntry, err error) error {
		t.Logf("saw %s", d.Name())
		if d.Name() == "helloworld.pdf" {
			hasPDF = true
			return nil
		}
		if d.Name() == "helloworld.bbl" {
			hasBBL = true
			return nil
		}
		if d.Name() == "helloworld.log" {
			hasLOG = true
			return nil
		}
		return nil
	})
	if !hasPDF {
		t.Errorf("Missing pdf output.")
	}
	if !hasBBL {
		t.Errorf("Missing bbl output.")
	}
	if !hasLOG {
		t.Errorf("Missing log output.")
	}
}
