SHELL := /bin/bash

all: dirs util tex gotransdoc 
.PHONY: dockerconverter gotransdoc tex 

dirs:
	mkdir -p bin

util:
	go build -o bin/sign utils/sign.go
	go build -o bin/verify utils/verify.go
	go build -o bin/gentoken utils/gentoken.go

tex:
	go build -o bin/golatex server/texsrv.go

gotransdoc:
	go build -o bin/gotransdoc server/ssconvertsrv.go

test:
	source etc/environment && go test ./latex ./server -v

dockerimg: server/texsrv.go service/*.go latex/*.go docker/*
	GOOS=linux go build -o docker/golatex-linux server/texsrv.go
	cd docker && make

dockerconverter: server/ssconvertsrv.go service/*.go ssconvert/*.go tika/*.go libreoffice/*.go dockerconverter/*
	CGO_ENABLED=0 GOOS=linux go build -ldflags '-w -extldflags "-static"' -o dockerconverter/gotransdoc server/ssconvertsrv.go
	cd dockerconverter && make
