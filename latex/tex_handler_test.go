package latex

import (
	"gitlab.com/alex.r.miron/gotransdoc/service"
	"io/fs"
	"path/filepath"
	"testing"
)

func TestLatex(t *testing.T) {
	fh, post, err := service.PackOutput("../samples/helloworld")
	if err != nil {
		t.Logf("%s", err)
		return
	}
	defer post()
	dir, name, err := service.SaveInput("helloworld.zip", fh)
	if err != nil {
		t.Logf("%s", err)
		return
	}
	defer service.Cleanup(dir)

	ofh, post, outName, _ := runTexCompiler(dir, name)
	defer post()

	dir, _, _ = service.SaveInput(outName, ofh)
	defer service.Cleanup(dir)

	commands, _ := service.GetManifest(dir, outName)
	if len(commands) != 4 {
		t.Errorf("Commands don't match: %s", commands)
	}

	hasPDF := false
	hasBBL := false
	hasLOG := false
	filepath.WalkDir(dir, func(path string, d fs.DirEntry, err error) error {
		t.Logf("saw %s", d.Name())
		if d.Name() == "helloworld.pdf" {
			hasPDF = true
			return nil
		}
		if d.Name() == "helloworld.bbl" {
			hasBBL = true
			return nil
		}
		if d.Name() == "helloworld.log" {
			hasLOG = true
			return nil
		}
		return nil
	})
	if !hasPDF {
		t.Errorf("Missing pdf output.")
	}
	if !hasBBL {
		t.Errorf("Missing bbl output.")
	}
	if !hasLOG {
		t.Errorf("Missing log output.")
	}
}
