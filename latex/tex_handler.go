package latex

import (
	"errors"
	"gitlab.com/alex.r.miron/gotransdoc/service"
	"io"
	"io/fs"
	"net/http"
	"path/filepath"
	"strings"
)

var texCommandMap = map[string][]string{
	"xelatex":  []string{"xelatex", "-interaction=nonstopmode"},
	"pdflatex": []string{"pdflatex", "-interaction=nonstopmode"},
	"pdftex":   []string{"pdflatex", "-interaction=nonstopmode"},
	"bibtex":   []string{"bibtex"},
	"biblatex": []string{"bibtex"},
}

func getDefaultManifestData(dir string) []service.FileCommand {
	var manifestData []service.FileCommand

	filepath.WalkDir(dir, func(path string, d fs.DirEntry, err error) error {
		if strings.HasSuffix(d.Name(), ".tex") {
			manifestData = append(manifestData, service.FileCommand{"xelatex", []string{d.Name()},""}, service.FileCommand{"xelatex", []string{d.Name()},""})
		}
		return nil
	})
	return manifestData
}

func runTexCompiler(dir string, fname string) (io.ReadCloser, func(), string, error) {

	manifestData, err := service.GetManifest(dir, fname)

	if err != nil {
		return nil, nil, fname, err
	}
	if len(manifestData) == 0 {
		manifestData = getDefaultManifestData(dir)
	}
	if len(manifestData) == 0 {
		return nil, nil, fname, errors.New("could not determine what to do.")
	}

	for _, command := range manifestData {
		err := service.RunCmd(dir, &command, texCommandMap)
		if err != nil {
			return nil, nil, fname, err
		}
	}

	fh, fn, err := service.PackOutput(dir)
	return fh, fn, fname, err
}

func HandleLatex(w http.ResponseWriter, r *http.Request) error {
	if r.Method == "POST" {
		return service.HandlePOST(w, r, runTexCompiler)
	}
	return nil
}
