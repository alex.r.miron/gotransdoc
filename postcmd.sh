#!/bin/bash 

set -x
API=${API:-latex}
curl --insecure -D - -X POST -H "Authorization: $CLIENT_SECRET" -F "zipped_source=@$1" https://$SERVER_HOST:$SERVER_PORT/api/$API/ "${@:2}"
