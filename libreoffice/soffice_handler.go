package libreoffice

import (
	"errors"
	"gitlab.com/alex.r.miron/gotransdoc/service"
	"io"
	"io/fs"
	"net/http"
	"path/filepath"
	"strings"
)

var sofficeCommandMap = map[string][]string{
	"doc_to_pdf": []string{"soffice", "--headless", "--convert-to", "pdf", "--outdir", "."},
}

func getDefaultManifestData(dir string) []service.FileCommand {
	var manifestData []service.FileCommand

	filepath.WalkDir(dir, func(path string, d fs.DirEntry, err error) error {
		name := d.Name()
		ext := strings.ToLower(filepath.Ext(name))
		if len(ext) > 0 && ext != ".pdf" {
			manifestData = append(manifestData, service.FileCommand{"doc_to_pdf", []string{name},""})
		}
		return nil
	})
	return manifestData
}

func runSofficeCompiler(dir string, fname string) (io.ReadCloser, func(), string, error) {

	manifestData, err := service.GetManifest(dir, fname)

	if err != nil {
		return nil, nil, fname, err
	}
	if len(manifestData) == 0 {
		manifestData = getDefaultManifestData(dir)
	}

	if len(manifestData) == 0 {
		return nil, nil, fname, errors.New("could not determine what to do.")
	}

	for _, command := range manifestData {
		err := service.RunCmd(dir, &command, sofficeCommandMap)
		if err != nil {
			return nil, nil, fname, err
		}
	}

	fh, fn, err := service.PackOutput(dir)
	return fh, fn, fname, err
}

func HandleConvert(w http.ResponseWriter, r *http.Request) error {
	if r.Method == "POST" {
		return service.HandlePOST(w, r, runSofficeCompiler)
	}
	return nil
}
