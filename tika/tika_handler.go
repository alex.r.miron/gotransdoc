package tika

import (
	"errors"
	"fmt"
	"gitlab.com/alex.r.miron/gotransdoc/service"
	"io"
	"io/fs"
	"net/http"
	"path/filepath"
	"strings"
	"os"
)

var tikaCommandMap = map[string][]string{
	"tika": []string{"java", "-jar", fmt.Sprintf("%s/tika-app-1.18.jar",os.Getenv("TIKA_ROOT")), fmt.Sprintf("--config=%s/tika.xml", os.Getenv("TIKA_ROOT") ), "-J", "-x"},
}

func getDefaultManifestData(dir string) []service.FileCommand {
	var manifestData []service.FileCommand

	filepath.WalkDir(dir, func(path string, d fs.DirEntry, err error) error {
		name := d.Name()
		ext := strings.ToLower(filepath.Ext(name))
		if len(ext) > 0 {
			manifestData = append(manifestData, service.FileCommand{"tika", []string{name}, name+".tika.json"})
		}
		return nil
	})
	return manifestData
}

func runTikaCompiler(dir string, fname string) (io.ReadCloser, func(), string, error) {

	manifestData, err := service.GetManifest(dir, fname)

	if err != nil {
		return nil, nil, fname, err
	}
	if len(manifestData) == 0 {
		manifestData = getDefaultManifestData(dir)
	}

	if len(manifestData) == 0 {
		return nil, nil, fname, errors.New("could not determine what to do.")
	}

	for _, command := range manifestData {
		err := service.RunCmd(dir, &command, tikaCommandMap)
		if err != nil {
			return nil, nil, fname, err
		}
	}

	fh, fn, err := service.PackOutput(dir)
	return fh, fn, fname, err
}

func HandleConvert(w http.ResponseWriter, r *http.Request) error {
	if r.Method == "POST" {
		return service.HandlePOST(w, r, runTikaCompiler)
	}
	return nil
}
