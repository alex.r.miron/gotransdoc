package main

import (
	"fmt"
	"gitlab.com/alex.r.miron/gotransdoc/service"
	"os"
)

func main() {
	message := os.Args[1]
	signed, _ := service.Sign([]byte(message))
	fmt.Printf("%s\n", signed)

}
