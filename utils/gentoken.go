package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"gitlab.com/alex.r.miron/gotransdoc/service"
)

func main() {
	var expires = flag.String("expires", "0000-00-00", "expires at yyyy-mm-dd")
	flag.Parse()
	var token = service.AuthToken{*expires}
	message, _ := json.Marshal(token)
	signed, _ := service.Sign(message)
	fmt.Printf("%s\n", signed)

}
