package main

import (
	"fmt"
	"gitlab.com/alex.r.miron/gotransdoc/service"
	"os"
)

func main() {
	message := os.Args[1]
	verified, _ := service.DecodeVerify(message)
	fmt.Printf("%s\n", verified)

}
