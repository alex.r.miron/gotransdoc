#!/bin/bash

source /env
if [ -n "$SERVER_SSL_CERT" ]; then
 PROTO=https
else
 PROTO=http
fi

line=$(curl -s -k $PROTO://127.0.0.1:$SERVER_PORT/api/health/ | grep ok.)

if [ -z "$line" ] ; then
 exit 1
fi
exit 0
 
